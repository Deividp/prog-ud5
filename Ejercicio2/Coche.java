package UD5EJER2;

public class Coche {


    private String modelo;

    private String color;

    private boolean metalizado;

    private String matricula;

    private String tipo;

    private String fecha;

    private boolean seguroTodoRiesgo;



    public Coche(String modelo, String color, boolean metalizado, String matricula, String tipo ,String fecha ){

        this.modelo = modelo;
        this.color = color;
        this.metalizado = metalizado;
        this.matricula = matricula;
        this.tipo = tipo;
        this.fecha = fecha;


    }


    public void setSeguroTodoRiesgo (String seguroTodoRiesgo){

        if (seguroTodoRiesgo.equalsIgnoreCase("Si")){

            this.seguroTodoRiesgo = true;
        }else{

            this.seguroTodoRiesgo = false;
        }
    }

    public boolean isSeguroTodoRiesgo (boolean seguroTodoRiesgo){

        seguroTodoRiesgo = this.seguroTodoRiesgo;

        return seguroTodoRiesgo;
    }
    public String getModelo(){

        return this.modelo;
    }

    public String getColor(){
        color = this.color;

        return color;
    }

    public boolean isMetalizado() {

        metalizado = this.metalizado;
        return metalizado;
    }

    public boolean isSeguroTodoRiesgo() {
        seguroTodoRiesgo = this.seguroTodoRiesgo;
        return seguroTodoRiesgo;
    }

    public String getMatricula() {
        String matricula = this.matricula;
        return matricula;
    }

    public String getTipo() {

        tipo = this.tipo;
        return tipo;
    }

    public String getFecha() {

        fecha = this.fecha;
        return fecha;
    }

    public void mostrarInfo(){

        System.out.println("Modelo "+ this.getModelo() + ", color "+ this.getColor()+".");
    }
}

