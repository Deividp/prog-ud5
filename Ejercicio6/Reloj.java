package UD5EJER6;

public class Reloj {

    private int horas;

    private int minutos;

    private int segundos;

    Reloj( int horas, int minutos, int segundos){

        this.horas = horas;

        this.minutos = minutos;

        this.segundos = segundos;
    }

    Reloj(){

        this.horas = 0;
        this.minutos = 0;
        this.segundos = 0;

    }

    public void dime12Hora(){

        if( this.horas == 0){
            System.out.println("12:" +this.minutos+":" + this.segundos +"AM" );
        }else if(this.horas > 0 && this.horas < 12){
            System.out.println(this.horas +":"+ this.minutos +":"+ this.segundos + "AM");
        }else if(this.horas==12){

            System.out.println( this.horas +":"+this.minutos +":"+this.segundos+"PM");
        }else {
            System.out.println( this.horas-12 +":"+this.minutos +":"+this.segundos+"PM");
        }

    }

    public void dime24Hora(){
        System.out.println( this.horas +":"+this.minutos +":"+this.segundos+"");
    }
}
