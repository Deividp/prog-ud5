package UD5EJER6;

public class Main {

    public static void main(String[] args) {

        Reloj arena = new Reloj();
        Reloj piedra = new Reloj(11, 16, 22);

        arena.dime12Hora();
        arena.dime24Hora();

        piedra.dime12Hora();
        piedra.dime24Hora();
    }
}
