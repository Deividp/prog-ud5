package UD5EJER10;

import UD5EJER8.Persona;

public class Cuenta {

    int numero;
    double saldo;
    float interes;
    Persona personaje;

    public Cuenta (int numero, double saldo, float interes, Persona personaje){

        this.numero = numero;
        this.saldo = saldo;
        this.interes = interes;
        this.personaje = personaje;

    }

    public void ingreso( double ingreso){

        this.saldo = this.saldo + ingreso;
    }

    public void reintegro( double reintegro){

        this.saldo = this.saldo - reintegro;
    }
}
