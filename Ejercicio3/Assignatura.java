package UD5EJER3;

public class Assignatura {


        private String nom;
        private int codi;
        private int curs;

        public Assignatura(String nom, int codi, int curs) {
            this.nom = nom;
            this.codi = codi;
            this.curs = curs;
        }

        public String getNom() {
            this.nom = this.nom;
            return this.nom;
        }

        public int getCodi() {
            this.codi = this.codi;
            return this.codi;
        }

        public int getCurs() {
            this.curs = this.curs;
            return this.curs;
        }
    }


