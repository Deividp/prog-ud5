package UD5EJER7;

public class Fraccion {

    private int numerador;
    private int denominador;

    public Fraccion(int numerador , int denominador){

        this.numerador = numerador;
        this.denominador = denominador;

    }

    public Fraccion(int numerador){
        this.numerador = numerador;
        this.denominador = 1;

    }
    public Fraccion(){
        numerador = 0;
        denominador = 1;
    }

    public Fraccion sumar (Fraccion fraccion){
        int numerador = fraccion.numerador;
        int denominador = fraccion.denominador;
        int auxNumerador = 0;
        if( this.denominador == denominador){

             auxNumerador = numerador + this.numerador;
        }else{
            denominador = numerador * this.denominador;
            auxNumerador = this.numerador * denominador;
            numerador = numerador * this.denominador;
            auxNumerador = auxNumerador + numerador;


        }
        Fraccion laSuma = new Fraccion ( auxNumerador,  denominador);
        return laSuma;
    }

    public  Fraccion restar (Fraccion fraccion){
        int denominador = fraccion.denominador;
        int numerador = fraccion.numerador;
        int auxNumerador = 0;
        if( this.denominador == denominador){

            auxNumerador = this.numerador - numerador;
            denominador = this.denominador ;
        }else{
            denominador = denominador * this.denominador;
            auxNumerador = this.numerador * denominador;
            numerador = numerador * this.denominador;
            auxNumerador = auxNumerador - numerador;

        }
        Fraccion laResta = new Fraccion( auxNumerador , denominador  );

        return laResta;
    }

    public Fraccion multiplicar( Fraccion fraccion){

        int numerador = fraccion.numerador;
        int denominador = fraccion.denominador;
        numerador = this.numerador * numerador;
        denominador = this.denominador * denominador;

        Fraccion laMultiplica = new Fraccion( numerador, denominador );

        return laMultiplica;
    }

    public Fraccion dividir (Fraccion fraccion){
        int numerador = fraccion.numerador;
        int denominador = fraccion.denominador;

        int auxNumerador =  this.numerador * denominador;
        denominador = this.denominador*numerador;

        Fraccion laDivision = new Fraccion( auxNumerador, denominador );
        return laDivision;

    }
    
}
