package UD5EJER8;

public class Fecha {

    public int dia;
    public int mes;
    public int any;


    public Fecha(int dia, int mes, int any) {

        this.dia = dia;
        this.mes = mes;
        this.any = any;

    }


    public boolean verificarFecha() {

        boolean real = false;
        if ((this.mes == 1 ||
                this.mes == 3 ||
                this.mes == 5 ||
                this.mes == 7 ||
                this.mes == 10 ||
                this.mes == 12) &&
                (this.dia > 0 && this.dia <= 31)) {

            real = true;
        } else if ((this.mes == 4 ||
                this.mes == 6 ||
                this.mes == 9 ||
                this.mes == 11) &&
                (this.dia > 0 && this.dia <= 30)) {
            real = true;
        } else if (this.mes == 2 && this.dia > 0 && this.dia <= 28) {
            real = true;
        } else if ((this.any % 100 != 0 && this.any % 4 == 0 || this.any % 400 == 0) && this.dia == 29 ){
            real = true;
        }else{
            real = false;
        }

            return real;

    }

    public void sumarUnDia(){

        int dia = this.dia;
        int mes = this.mes;
        int any = this.any;

        if ((mes == 1 ||
                mes == 3 ||
                mes == 5 ||
                mes == 7 ||
                mes == 10 ||
                mes == 12) &&
                (dia > 0 && dia < 31)) {
            dia++;
        } else if ((mes == 4 ||
                mes == 6 ||
                mes == 9 ||
                mes == 11) &&
                (dia > 0 && dia < 30)) {
                 dia++;
        } else if (mes == 2 && dia > 0 && dia < 28) {
            dia++;
        } else if ((this.any % 100 != 0 && this.any % 4 == 0 || this.any % 400 == 0) && this.dia == 28 ){
            dia++;
        }

        if (dia == this.dia && mes == 12){
            dia=1;
            mes=1;
            any++;
    }

        if(dia == this.dia){
            dia = 1;
            mes++;

        }

        System.out.println(dia + "/" + mes + "/" + any);


    }

   public String toString(){

       return this.dia +"/"+ this.mes +"/"+ this.any;


   }




}
