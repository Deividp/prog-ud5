package UD5EJER4;

public class NIF {

    private int num;
    private char letra;

    public NIF (int num){
        this.num = num;
        letra = buscaLetra();

    }

    private char buscaLetra(){
        String listaLetras = "TRWAGMYFPDXBNJZSQVHLCKE";
        int modulo = this.num % 23;
         return listaLetras.charAt(modulo);

    }

    public char getLetra() {
        return letra;
    }
    public int getnum(){

        return this.num;
    }
}
