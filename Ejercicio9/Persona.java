package UD5EJER8;

public class Persona {

    private Fecha fechaNacimiento;

    private String nombre;

    private String primerApellido;

    private String segonApellido;

    private String dni;

    private String direccion;

    private String email;


    public Persona(Fecha fechaNacimiento, String nombre, String primerApellido, String segonApellido, String dni, String direcion, String email){

        this.fechaNacimiento = fechaNacimiento;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segonApellido = segonApellido;
        this.dni = dni;
        this.direccion = direcion;
        this.email = email;

    }

    public Persona(Fecha fechaNacimiento, String nombre, String primerApellido, String segonApellido, String dni, String direcion){

        this.fechaNacimiento = fechaNacimiento;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segonApellido = segonApellido;
        this.dni = dni;
        this.direccion = direcion;


    }
    public Persona(Fecha fechaNacimiento, String nombre, String dni){

        this.fechaNacimiento = fechaNacimiento;
        this.nombre = nombre;
        this.dni = dni;


    }
}


